/**
* @author Nikolas Dimitrio Badani Gasdaglis 20092 
* @fecha 16/5/2021
* @seccion Algoritmos y Estructuras de Datos 20
* @class Paciente
*/

public class Paciente {
	private String nombre;
	private Fichas fichas
	private String sintoma;
	private int prioridad; 

	public Paciente(String nombre, String sintoma, int prioridad, String fichas) {
		this.nombre = nombre;
		this.sintoma = sintoma;
		this.prioridad = prioridad;
		this.ficha = ficha; 
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSintoma() {
		return sintoma;
	}

	public void setSintoma(String sintoma) {
		this.sintoma = sintoma;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(int prioridad) {
		this.prioridad = prioridad;
	}

	public String getFicha() {
		return ficha;
	}

	public void setFicha(Fichas fichas) {
		this.ficha = ficha;
	}
}





