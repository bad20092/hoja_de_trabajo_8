/**
* @author Nikolas Dimitrio Badani Gasdaglis 20092 
* @fecha 16/5/2021
* @seccion Algoritmos y Estructuras de Datos 20
* @class Fichas
*/

public class Fichas {

	private int atender;
	private int codigo; 

	public Fichas(int atender, int codigo) {
		this.atender = atender;
		this.codigo = codigo;
	}

	public int getAtender() {
		return atender;
	}

	public void setAtender(int atender) {
		this.atender = atender;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
}