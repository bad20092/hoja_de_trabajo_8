/**
* @author Nikolas Dimitrio Badani Gasdaglis 20092 
* @fecha 16/5/2021
* @seccion Algoritmos y Estructuras de Datos 20
* @class Main
*/

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class Main {

	public static void main(String[] args) {

		Stack<Fichas> a = new Stack<Fichas>();
		Stack<Fichas> b = new Stack<Fichas>();
		Stack<Fichas> c = new Stack<Fichas>();
		Stack<Fichas> d = new Stack<Fichas>();
		Stack<Fichas> e = new Stack<Fichas>();

		// Llenar 

		for (int i=1; i <= 20; i++) {
			a.push(new Fichas(Prioridad.ATENDER_PRIMERO, i ));
			b.push(new Fichas(Prioridad.ATENDER_SEGUNDO, i ));
			c.push(new Fichas(Prioridad.ATENDER_TERCERO, i ));
			d.push(new Fichas(Prioridad.ATENDER_CUARTO, i ));
			e.push(new Fichas(Prioridad.ATENDER_ULTIMO, i ));
		}

		System.out.println("Atender de Primero");
		mostrar(a);
		System.out.println("Atender de Segundo");
		mostrar(b);
		System.out.println("Atender de Tercero");
		mostrar(c);
		System.out.println("Atender de Cuarto");
		mostrar(d);
		System.out.println("Atender de Ultimo");
		mostrar(e);

		Queue<Paciente> pacientes = new LinkedList<Paciente>();

		pacientes.add(new Paciente("Nombre1", Sintoma.sintoma, Prioridad.ATENDER_PRIMERO));
		pacientes.add(new Paciente("Nombre2", Sintoma.sintoma, Prioridad.ATENDER_SEGUNDO));
		pacientes.add(new Paciente("Nombre3", Sintoma.sintoma, Prioridad.ATENDER_TERCERO));
		pacientes.add(new Paciente("Nombre4", Sintoma.sintoma, Prioridad.ATENDER_CUARTO));
		pacientes.add(new Paciente("Nombre5", Sintoma.sintoma, Prioridad.ATENDER_ULTIMO));

		System.out.println("PACIENTES");
		mostrar(pacientes);

		// ASIGNAR LAS FICHAS A LOS PACIENTES 
		asignarFichas(a,b,c,d,e, pacintes);

		System.out.println("Atender de Primero");
		mostrar(a);
		System.out.println("Atender de Segundo");
		mostrar(b);
		System.out.println("Atender de Tercero");
		mostrar(c);
		System.out.println("Atender de Cuarto");
		mostrar(d);
		System.out.println("Atender de Ultimo");
		mostrar(e);

		// Calcular la prioridad del paciente para que sea atendido 
		calcularPrioridadPaciente(a,b,c,d,e);

		// Realizar el metodo para atender a los pacientes conforme se encuentren en sus fichas 

		Paciente pa = pacintes.remove();
		System.out.println("> paciente: " + pa.getNombre());
		atender(pa, a, b, c, d, e);
	}

	public static void mostrar(Queue<Paciente> cola) {
		int n = cola.size();
		for(int i = 0, i < n; i++)
			Pciente pa = cola.remove();
			System.out.println(" Paciente : " + pa.getNombre() + "Sintoma : " + pa.getSintoma() + "Prioridad : " + pa.getPrioridad() + );
			cola.add(pa)
		}
	}






