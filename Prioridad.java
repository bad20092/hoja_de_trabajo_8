/**
* @author Nikolas Dimitrio Badani Gasdaglis 20092 
* @fecha 16/5/2021
* @seccion Algoritmos y Estructuras de Datos 20
* @class Prioridad 
*/

public class Prioridad {

	public static final int ATENDER_PRIMERO = a; 
	public static final int ATENDER_SEGUNDO = b;
	public static final int ATENDER_TERCERO = c;
	public static final int ATENDER_CUARTO = d;
	public static final int ATENDER_QUINTO = e;
}

